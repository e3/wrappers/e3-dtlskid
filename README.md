## e3-dtlskid

ESS Site-specific EPICS module : dtlskid


### Description
This repository contains all the code developed for _**DTL Cooling control system**_. The support provide the layer required to manage the communication to the Skid PLC (Siemens S7-1500) and to define the DTL Cooling Functional State Machine


### Main files
The principal files this E3 module provides are the following:

1. **Database files**
    * countdown.db
    * cwm-cws04_ctrl-plc-001-sync.db
    * cwm-cws04_ctrl-plc-001-sync.substitutions
    * cwm-cws04_ctrl-plc-001-sync.template
    * dbCoolingResetMaintenance.db
    * dtlCoolingControl.db
    * stateMachine.db
2. **Sources files**
    * communication_management.st
    * cooling_statemachine.st
3. **Script files**
    * postInitFluxAlarms.cmd
    * postInitPressureAlarms.cmd
    * postInitTemperatureAlarms.cmd
4. **Iocsh files**
    * dtlskid.iocsh



  ### References

  The module is based on the work done by Maurizio Montis (INFN-LNL) and available on GitHub:
  **[e3-dtlSkid](https://github.com/mauriziomontis/e3-dtlSkid)**

  For support: maurizio.montis@lnl.infn.it
